# Dao Style Vue Maintainer 文件

此文件描述了运营项目 daocloud/dao-style-vue 的负责人(也称Maintainer)。
 
Maintainer的职责有：

* 诊断分类用户issue，[详见/Issue-分类.md](project/Issue-分类.md)；
* 分类审核用户PR，[详见/PR-审核流程.md](project/PR-审核流程.md)；
* 制定项目的roadmap，[详见/ROADMAP.md](ROADMAP.md)；
* 维护项目的其他内容，如：CHANGELOG.md，版本发布等。


[Org]

	[Org."核心维护者"]
		people = {
			"cinderyx": [
				"button", 
				"card", 
				"input", 
				"pseudo-disable",
			],
			"jamiefang":[],
			"lsq645599166": [
				"checkbox", 
				"color", 
				"feedback", 
				"select",
			],
			"mimoning": [
				"icon-with-text", 
				"popover", 
				"radio", 
				"switch",
			],
			"qar": [
				"chip", 
				"clipborad", 
				"dropdown", 
				"list-group",
			],
			"tanbowensg": [
				"autocomplete", 
				"layout", 
				"progress", 
				"table",
			],
			"vivian-xu": [
				"editor", 
				"select-all", 
				"svg", 
				"tab",
			],
			"yank1": [
				"progress",
			],
			"Ye-Ting": [],
			"ZhuJingSi": [
				"callout", 
				"dialog", 
				"multistep", 
				"setting-layout",
			],
		}

	[Org."文档维护者"]
		people = [
			"allencloud",
		]

[people]

以下列表罗列所有和本项目相关的人员。若您参与了本项目的管理与维护，请按照GitHub ID的字典顺序将您的信息加入列表。

**本项目衷心地感谢所有参与者对项目的贡献。**

	[people.allencloud]
	Name = "Allen Sun"
	Email = "allen.sun@daocloud.io"
	GitHub = "allencloud"

	[people.cinderyx]
	Name = "CIN"
	Email = "cin.hu@daocloud.io"
	GitHub = "cinderyx"

	[people.jamiefang]
	Name = "Jamie Fang"
	Email = "jamie.fang@daocloud.io"
	GitHub = "jamiefang"

	[people.lsq645599166]
	Name = "Henry Liu"
	Email = "henry.liu@daocloud.io"
	GitHub = "lsq645599166"

	[people.mimoning]
	Name = "MimoN"
	Email = "mimo.ouyang@daocloud.io"
	GitHub = "mimoning"

	[people.qar]
	Name = "Qiao Anran"
	Email = "anran.qiao@daocloud.io"
	GitHub = "qar"

	[people.tanbowensg]
	Name = "tanbowensg"
	Email = "bowen.tan@daocloud.io"
	GitHub = "tanbowensg"

	[people.vivian-xu]
	Name = "vivian-xu"
	Email = "vivian.zhang@daocloud.io"
	GitHub = "vivian-xu"

	[people.yank1]
	Name = "Kay Yan"
	Email = "kay.yan@daocloud.io"
	GitHub = "yank1"

	[people.Ye-Ting]
	Name = "Ting Ye"
	Email = "ting.ye@daocloud.io"
	GitHub = "Ye-Ting"

	[people.ZhuJingSi]
	Name = "Cisy"
	Email = "cisy.zhu@daocloud.io"
	GitHub = "ZhuJingSi"
