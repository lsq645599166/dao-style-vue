# PR-提交规范

不论是开源，还是闭源，一个优秀项目的发展永远都离不开优秀的**协作**。而PR(Pull Request)意味着一名开发工程师对项目的完善作出的卓越努力。在这里，DaoCloud在大家的帮助下建立了《PR-提交规范》，望所有的参与者完善。

从工程师发现一个问题，到提交一个优质的PR，一般会经历以下5个步骤：

* 创建开发分支；
* 更新相应项目内容（代码，文档，测试等）；
* 提交commit信息；
* 向项目push分支；
* 发起PR，完善PR信息

## 1. 分支规范（branch）

为此项目提交代码贡献的前提之一，必须创建一个全新的开发分支，并根据您所需要完成的功能与类型来定义此分支，主要目标是帮助他人更好的理解分支的作用：

* 如果这是一个Bug修复分支，请按照下列规范命名提交分支：`bug/XXXX`，在这里XXXX是bug的精简完备描述；
* 如果这是一个新增feature分支，请按照下列规范命名提交分支：`feature/XXXX`，在这里XXXX是feature的精简完备描述；
* 如果这是一个功能增强分支，请按照下列规范命名提交分支：`enhancement/XXXX`，在这里XXXX是feature增强的精简完备描述；
* 如果这是一个文档更新分支，请按照下列规范命名提交分支：`docs/XXXX`，在这里XXXX是文档更新的精简完备描述；

## 2. 分支更新规范

分支更新有可能涉及到代码，文档，测试等。工程师更新的代码等应该符合以下的要求：

* 必要的情况拥有起码的代码注释；
* 代码越精简越好；
* 50行直接的代码，比起10行有效但是没人可以读懂的代码，要好；
* 组件越少越好，是否有必要新增一个class；
* 可以立即更新的内容，不要以后再来完成；
* 当面临两种选择时，尽量选择回滚比较简单的那种； 

## 3. 提交信息规范（commit message）

提交信息是帮助代码审核者更高效知晓PR意图的信息，可以有效加快 Code Review 的流程。我们推荐贡献者使用**简明清晰**的提交信息, 我们反对**寓意不明，指代含糊**的提交信息。一般情况下，我们鼓励使用这种形式的提交信息：

* [docs]xxxx：代表涉及文档
* [feature]xxxx：代表涉及功能特性
* [code]xxxx：代表涉及代码
* [Refactor]xxxx：代表重构内容xxxx
* [Fix]xxxx: 代表修复某个具体的内容xxxx
* 以及其他可读性强的表达方式

我们极度反对使用如下的提交信息，理由很简单:这样的提交信息非常不具有可读性。

* ~~fix bug~~
* ~~update~~

## 4. 向项目push分支

工程师完成本地的开发之后，需要将新建的分支，push到远程项目中。

## 5. PR描述规范

我们认为PR是一个阶段性的工作成果，PR作为代码合并的一个整体，同样需要您在可读性，可审核性方面按照相应的要求来完成。PR的主要要求如下：

* 单个PR一般只包含一个commit，特殊情况下，单个PR可以包含多个commit，但数量**不能超过5个**；
* PR的描述有助于代码审核者快速了解PR的性质与具体情况，我们**极度不赞成**内容为空的PR描述。

一个优秀完备的PR描述，拥有以下PR的形式：

```
PR 描述:
针对问题233，此PR加入功能特性A，为此项目实现B

我做了什么：
1. 修改A程序的程序逻辑，使得考虑特殊情况B；
2. 为以上的程序逻辑添加了一个测试案例；
3. 将特殊情况B，整理成案例，加入文档C。

我是如何做到的：
1. 在文件D中，判断E逻辑的时候，考虑特殊情况B；

如何验证这功能：
1. 在运行该项目的时候，输入内容F，可以得到正确结果G，即可验证

```

如您对此规范有问题，请feel free以PR的形式来完善该文档，项目的maintainer会按照实际情况来权衡利弊，共同来建立完善流程与规范。
