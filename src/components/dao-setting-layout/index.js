import daoSettingLayout from './dao-setting-layout.vue';
import daoSettingSection from './dao-setting-section.vue';
import daoSettingItem from './dao-setting-item.vue';

daoSettingLayout.Section = daoSettingSection;
daoSettingLayout.Item = daoSettingItem;

export default daoSettingLayout;
