import daoDialog from './dao-dialog.vue';
import daoDialogHeader from './dao-dialog-header/dao-dialog-header.vue';
import daoDialogStep from './dao-dialog-step/dao-dialog-step.vue';

daoDialog.Header = daoDialogHeader;
daoDialog.Step = daoDialogStep;

export default daoDialog;
