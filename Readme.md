# dao-style-vue

dao-style-vue 是一个基于 Vue 2.0 框架的 web UI 组件库。

## 引导链接

- [主页]()
- [文档](docs)
- [常见问题FAQ](FAQ.md)
- 项目参与
  - [问题反馈](https://github.com/DaoCloud/dao-style-vue/issues/new)
  - [贡献指南](project)
  - [测试](test/README.md)
- 安装部署
  - [快速安装](#快速安装)
  - [示例代码](#示例代码)
- 关于项目
  - [ROADMAP](ROADMAP.md)
  - [变更日志CHANGELOG](CHANGELOG.md)
  - [项目参与者](MAINTAINER.md)
  - [模块主要负责人](README.md#模块主要负责人)
  - [License](README.md#LICENSE)

## 快速安装

## 示例代码

## 浏览器支持

|浏览器类型|浏览器版本支持|
|--------|---------|
| Chrome| |
| Safari| |
| firefox||
| IE     ||

## 贡献指南

如果您仅仅是使用 dao-style-vue，请跳过此部分。

## 常见问题FAQ（frequently asked questions）

## 模块主要负责人

<table>
  <thead>
    <th>负责人</th>
    <th colspan="4">组件</th>
  </thead>
  <tbody>
    <tr>
      <td>欧阳宁</td>
      <td>icon-with-text</td>
      <td>popover</td>
      <td>radio</td>
      <td>switch</td>
    </tr>
    <tr>
      <td>谈博文</td>
      <td>table</td>
      <td>autocomplete</td>
      <td>progress</td>
      <td>layout</td>
    </tr>
    <tr>
      <td>张晓旭</td>
      <td>tab</td>
      <td>svg</td>
      <td>editor</td>
      <td>select-all</td>
    </tr>
    <tr>
      <td>刘世奇</td>
      <td>color</td>
      <td>checkbox</td>
      <td>feedback</td>
      <td>select</td>
    </tr>
    <tr>
      <td>朱静思</td>
      <td>multistep</td>
      <td>callout</td>
      <td>setting-layout</td>
      <td>dialog</td>
    </tr>
    <tr>
      <td>胡心悦</td>
      <td>button</td>
      <td>input</td>
      <td>pseudo-disable</td>
      <td>card</td>
    </tr>
    <tr>
      <td>乔安然</td>
      <td>dropdown</td>
      <td>list-group</td>
      <td>chip</td>
      <td>clipboard</td>
    </tr>
    <tr>
      <td>颜开</td>
      <td>progress</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

## LICENSE

待定
